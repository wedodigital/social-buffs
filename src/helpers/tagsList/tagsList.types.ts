export interface TagsList {
  nodes: {
    name: string
  }[]
}
