import { TagsList } from './tagsList.types'

function tagsList(tags: TagsList): string[] {
  return tags?.nodes.map((tag, index) => {
    return `${tag.name}${index + 1 !== tags.nodes.length ? ', ' : ''}`
  })
}

export default tagsList
