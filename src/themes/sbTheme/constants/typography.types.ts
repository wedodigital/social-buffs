export type ParagraphKeys = 1 | 2 | 3
export type HeadingSizeKeys = 1 | 2 | 3 | 4
export type WeightKeys = 1 | 2 | 3

export interface TypographyValues {
  fontSizeMobile: string
  lineHeightMobile: string
  fontSize: string
  lineHeight: string
}

export type Paragraph = {
  [P in ParagraphKeys]: TypographyValues
}

export type Heading = {
  [P in HeadingSizeKeys]: TypographyValues
}

export interface Typography {
  paragraph: Paragraph
  heading: Heading
  fontWeight: {
    [P in WeightKeys]: number
  }
  fontFamily: {
    body: string
    feature: string
  }
}
