export type colorsTypeKeys = 'black' | 'grey' | 'gold' | 'white' | 'midGrey'

export type Colours = {
  [key in colorsTypeKeys]: string
}
