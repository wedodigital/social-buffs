import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledResultsGridProps } from './ResultsGrid.style.types'

export const ResultsGrid = styled.div((props: StyledResultsGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-wrap: wrap;
  margin: ${props.theme.spacing.fixed[4]}px -${props.theme.spacing.fixed[2]}px -${props.theme.spacing.fixed[4]}px;
`)

export const Result = styled.div((props: StyledResultsGridProps): FlattenSimpleInterpolation => css`
  width: calc(50% - ${props.theme.spacing.fixed[2] * 2}px);
  margin: ${props.theme.spacing.fixed[4]}px ${props.theme.spacing.fixed[2]}px;

  ${props.theme.mixins.respondTo.md(css`
    width: calc(25% - ${props.theme.spacing.fixed[2] * 2}px);
  `)}
`)

export const CompanyLogo = styled.img((props: StyledResultsGridProps): FlattenSimpleInterpolation => css`
  display: block;
  max-height: ${props.theme.spacing.fixed[5]}px;
`)
