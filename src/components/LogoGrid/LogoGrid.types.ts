export interface LogoGridProps {
  logos: {
    logo: {
      sourceUrl: string
    }
  }[]
}
