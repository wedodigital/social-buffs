import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFullPageFeatureProps } from './FullPageFeature.style.types'

export const Wrapper = styled.div((props: StyledFullPageFeatureProps): FlattenSimpleInterpolation => css`
  width: 100%;
  overflow: hidden;
  position: relative;
  height: 100vh;
  margin-top: -${(props.theme.spacing.fixed[5] * 2) + props.theme.spacing.fixed[2]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-top: -${(props.theme.spacing.fixed[6] * 2) + props.theme.spacing.fixed[3]}px;
  `)}
`)

export const FullPageFeature = styled.div((props: StyledFullPageFeatureProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.white};
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  height: 100vh;
  width: 100%;
  overflow: hidden;
  position: relative;

  &::after {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: linear-gradient(to bottom, rgba(19, 20, 21, 0) 0%, rgba(19, 20, 21, .75) 100%);
  }
`)

export const Content = styled.div((): FlattenSimpleInterpolation => css`
  position: absolute;
  z-index: 20;
  text-align: center;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
`)

export const BackgroundVideo = styled.video((): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  z-index: -100;
  transform: translateX(-50%) translateY(-50%);
  background-size: cover;
`)

export const VideoOverlay = styled.div((): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  background: rgba(0, 0, 0, 0.2);
`)
