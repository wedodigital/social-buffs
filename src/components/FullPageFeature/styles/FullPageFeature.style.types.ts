import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledFullPageFeatureProps {
  theme: Theme;
  backgroundImage: string
}
