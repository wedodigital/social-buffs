import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import Container from '@components/Container'

import tagsList from '@helpers/tagsList'

import * as Styled from './styles/FullPageFeature.style'

import { FullPageFeatureProps } from './FullPageFeature.types'

const FullPageFeature: FC<FullPageFeatureProps> = ({
  title,
  backgroundImage,
  categories,
  titleHighlight,
  titleSuffix,
  backgroundVideo,
}: FullPageFeatureProps): ReactElement => {
  return (
    <Styled.Wrapper>
      <Styled.FullPageFeature backgroundImage={backgroundImage} />
      <Styled.Content>
        <Container width='mid'>
          <Heading
            text={title}
            highlightText={titleHighlight}
            suffixText={titleSuffix}
            size={4}
            level={1}
          />
          <If condition={categories?.nodes.length}>
            <Heading size={1} text={tagsList(categories)} fontStyle='body' />
          </If>
        </Container>
      </Styled.Content>
      <If condition={backgroundVideo}>
        <Styled.VideoOverlay />
        <Styled.BackgroundVideo autoPlay loop muted playsInline poster={backgroundVideo.poster}>
          <source src={backgroundVideo.mp4} type="video/mp4" />
          <source src={backgroundVideo.webm} type="video/webm" />
        </Styled.BackgroundVideo>
      </If>
    </Styled.Wrapper>
  )
}

export default FullPageFeature
