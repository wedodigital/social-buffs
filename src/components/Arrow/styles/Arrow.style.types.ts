import { Theme } from '@themes/sbTheme/sbTheme.types'

import { ArrowProps } from '../Arrow.types'

export interface StyledArrowProps {
  theme: Theme;
  direction: ArrowProps['direction']
}
