// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
/* eslint-disable react/react-in-jsx-scope */

/** *************************************************************************** */
/** Import - helpers                                                            */
/** --------------------------------------------------------------------------- */
/** Imports that are not being mocked or tested                                 */
/** *************************************************************************** */

/** *************************************************************************** */
/** Import - tested files                                                       */
/** --------------------------------------------------------------------------- */
/** Imports that unit tests will be written against                             */
/** *************************************************************************** */
import Arrow from '../Arrow'

import MatchMedia from '@components/MatchMedia'

/** *************************************************************************** */
/** Import - mocked files                                                       */
/** --------------------------------------------------------------------------- */
/** Imports that are defined only to be mocked eg stores, utils. helpers        */
/** *************************************************************************** */

/** *************************************************************************** */
/** Jest mocks                                                                  */
/** --------------------------------------------------------------------------- */
/** Globally defined jest mocks                                                 */
/** *************************************************************************** */

/** *************************************************************************** */
/** Unit testing                                                                */
/** *************************************************************************** */

describe('<Arrow />', () => {
  describe('render', () => {
    it('should not render without an onClick', () => {
      const wrapper = shallow(<Arrow />)
      expect(wrapper.find(MatchMedia).length).toBe(0)
    })

    it('should render when an onClick is passed', () => {
      const wrapper = shallow(<Arrow onClick={() => jest.fn()} />)
      expect(wrapper.find(MatchMedia).length).toBe(1)
    })
  })
})
