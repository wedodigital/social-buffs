export interface ArrowProps {
  direction: 'next' | 'prev'
  onClick?: () => void
}
