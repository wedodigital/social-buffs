import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledFooterProps {
  theme: Theme;
}
