import React, { ReactElement, FC } from 'react'

import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/LargeTextSection.style'

import { LargeTextSectionProps } from './LargeTextSection.types'

const LargeTextSection: FC<LargeTextSectionProps> = ({
  content,
}: LargeTextSectionProps): ReactElement => {
  return (
    <Styled.LargeTextSection>
      <RawHtmlWrapper content={content} />
    </Styled.LargeTextSection>
  )
}

export default LargeTextSection
