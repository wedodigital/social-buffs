import { ReactNode } from 'react'

export interface ImageCarouselProps {
  /**
   * React children
   * */
  children: ReactNode;
}
