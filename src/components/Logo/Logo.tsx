import React, { FC, ReactElement } from 'react'

import LogoSVG from '@assets/svg/socialBuff.svg'

import * as Styled from './styles/Logo.style'

const Logo: FC = (): ReactElement => {
  return (
    <Styled.Logo to='/'>
      <LogoSVG />
    </Styled.Logo>
  )
}

export default Logo
