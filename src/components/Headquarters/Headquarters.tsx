import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'
import Link from '@components/Link'
import MatchMedia from '@components/MatchMedia'

import * as Styled from './styles/Headquarters.style'

import { HeadquartersProps } from './Headquarters.types'

const Headquarters: FC<HeadquartersProps> = ({
  title,
  content,
  address,
  email,
  telephoneNumber,
  image,
}: HeadquartersProps): ReactElement => {
  return (
    <>
      <MatchMedia breakpoint='sm'>
        <Heading size={3} text={title} />
      </MatchMedia>
      <Styled.Headquarters>
        <Styled.HeadquartersContent>
          <MatchMedia breakpoint='md' andAbove>
            <Heading size={3} text={title} />
          </MatchMedia>
          <Styled.ContentBlock altAppearance={true}>
            <RawHtmlWrapper content={content} />
          </Styled.ContentBlock>
          <Styled.ContentBlock altAppearance={false}>
            <RawHtmlWrapper content={address} />
          </Styled.ContentBlock>
          <Styled.ContentBlock altAppearance={false}>
            <Link href={`mailto:${email}`} text={email} />
            <Link href={`tel:${telephoneNumber}`} text={telephoneNumber} />
          </Styled.ContentBlock>
        </Styled.HeadquartersContent>
        <Styled.HeadQuartersImage backgroundImage={image} />
      </Styled.Headquarters>
    </>
  )
}

export default Headquarters
