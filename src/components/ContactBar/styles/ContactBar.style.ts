import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledContactBarProps } from './ContactBar.style.types'

export const ContactBar = styled.div((props: StyledContactBarProps): FlattenSimpleInterpolation => css`
  background: rgba(19, 20, 21, .75);
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  z-index: 10;
  display: flex;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[2]}px 0;
  `)}

  & > [class*=Container] {
    display: flex;
    align-items: center;
    justify-content: flex-end;

    & > * {
      margin-right: ${props.theme.spacing.fixed[4]}px;

      &:last-child {
        margin-right: 0;
      }
    }
  }
`)

export const MobileButton = styled.a((props: StyledContactBarProps): FlattenSimpleInterpolation => css`
  display: inline-block;
  text-align: center;
  color: ${props.theme.colours.white};
  text-decoration: none;
  padding: ${props.theme.spacing.fixed[3]}px ${props.theme.spacing.fixed[2]}px;
  flex-grow: 1;
  font-family: ${props.theme.typography.fontFamily.feature};

  ${props.appearance === 'primary' && css`
    background: ${props.theme.colours.gold};
  `}
`)
