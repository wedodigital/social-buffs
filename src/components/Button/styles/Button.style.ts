import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledButtonProps } from './Button.style.types'

export const Button = styled.div((props: StyledButtonProps): FlattenSimpleInterpolation => css`
  cursor: pointer;
  color: ${props.theme.colours.white};
  text-transform: uppercase;
  background: ${props.theme.colours.gold};
  border: none;
  padding: ${props.theme.spacing.fixed[2]}px ${props.theme.spacing.fixed[5]}px;
  text-decoration: none;
  border-radius: ${props.theme.spacing.fixed[1]}px;
  transition: .4s all ease;
  font-size: ${props.theme.typography.paragraph[2].fontSizeMobile};
  line-height: ${props.theme.typography.paragraph[2].lineHeightMobile};
  font-family: ${props.theme.typography.fontFamily.feature};

  ${props.theme.mixins.respondTo.md(css`
    font-size: ${props.theme.typography.paragraph[2].fontSize};
    line-height: ${props.theme.typography.paragraph[2].lineHeight};
  `)}

  &:hover {
    opacity: .8;
  }
`)
