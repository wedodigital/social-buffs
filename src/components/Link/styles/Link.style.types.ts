import { Theme } from '@themes/sbTheme/sbTheme.types'

import { LinkProps } from '../Link.types'

export interface StyledLinkProps {
  theme: Theme;
  appearance: LinkProps['appearance']
  fontStyle: LinkProps['fontStyle']
}
