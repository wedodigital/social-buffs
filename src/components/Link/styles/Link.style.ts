import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledLinkProps } from './Link.style.types'

export const Link = styled.div((props: StyledLinkProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.white};
  text-decoration: none;

  ${props.appearance === 'secondary' && css`
    color: ${props.theme.colours.gold};
    text-decoration: underline;
  `}

  ${props.fontStyle === 'feature' && css`
    font-family: ${props.theme.typography.fontFamily.feature};
  `}
`)
