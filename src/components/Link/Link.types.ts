export interface LinkProps {
  to?: string
  href?: string
  text: string
  appearance?: 'primary' | 'secondary'
  fontStyle?: 'feature' | 'body'
}
