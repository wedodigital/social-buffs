import React, { PureComponent, ReactNode } from 'react'
import { Link as GatsbyLink } from 'gatsby'

import * as Styled from './styles/Link.style'

import { LinkProps } from './Link.types'

export default class Link extends PureComponent<LinkProps> {
  public static defaultProps: Partial<LinkProps> = {
    fontStyle: 'body',
  }

  private get linkType() {
    if (this.props.href) {
      return 'a'
    }
    return GatsbyLink
  }



  render(): ReactNode {
    return (
      <Styled.Link
        as={this.linkType}
        to={this.props.to}
        href={this.props.href}
        appearance={this.props.appearance}
        fontStyle={this.props.fontStyle}
      >
        {this.props.text}
      </Styled.Link>
    )
  }
}
