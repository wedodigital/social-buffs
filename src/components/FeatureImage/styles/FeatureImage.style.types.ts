import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledFeatureImageProps {
  theme: Theme
  backgroundImage: string
}
