import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledPartnerTileProps } from './PartnerTile.style.types'

export const PartnerTile = styled.div((props: StyledPartnerTileProps): FlattenSimpleInterpolation => css`
  width: calc(50% - ${props.theme.spacing.fixed[2] * 2}px);
  margin: ${props.theme.spacing.fixed[2]}px;
  padding: ${props.theme.spacing.fixed[3]}px;
  background: ${props.theme.colours.midGrey};
  aspect-ratio: 1 / 1;
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  text-align: center;

  ${props.theme.mixins.respondTo.md(css`
    width: calc(25% - ${props.theme.spacing.fixed[2] * 2}px);
    padding: ${props.theme.spacing.fixed[8]}px;
  `)}

  img {
    display: block;
    margin-bottom: ${props.theme.spacing.fixed[1]}px;
  }
`)
