import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeaderProps } from './Header.style.types'

export const Header = styled.div((props: StyledHeaderProps): FlattenSimpleInterpolation => css`
  z-index: 20;
  position: relative;

  .headroom {
    padding: ${props.theme.spacing.fixed[5]}px 0;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: center;

    ${props.theme.mixins.respondTo.md(css`
      padding: ${props.theme.spacing.fixed[6]}px 0;
    `)}

    &::after {
      content: '';
      display: block;
      background: rgba(19, 20, 21, .75);
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      transition: 0.4s all ease;
    }

    &--unfixed {
      &::after {
        background: none;
      }
    }

    & > * {
      position: relative;
      z-index: 20;
    }
  }
`)
