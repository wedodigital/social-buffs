import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledHeaderProps {
  theme: Theme;
}
