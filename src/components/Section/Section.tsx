import React, { ReactElement, FC } from 'react'

import Container from '@components/Container'

import * as Styled from './styles/Section.style'

import { SectionProps } from './Section.types'

const Section: FC<SectionProps> = ({
  children,
  titleSection,
  paddingLevel = 1
}: SectionProps): ReactElement => {
  return (
    <Styled.Section titleSection={titleSection} paddingLevel={paddingLevel}>
      <Container>
        {children}
      </Container>
    </Styled.Section>
  )
}

export default Section
