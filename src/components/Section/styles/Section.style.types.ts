import { Theme } from '@themes/sbTheme/sbTheme.types'

import { SectionProps } from '../Section.types'

export interface StyledSectionProps {
  theme: Theme
  titleSection: boolean
  paddingLevel: SectionProps['paddingLevel']
}
