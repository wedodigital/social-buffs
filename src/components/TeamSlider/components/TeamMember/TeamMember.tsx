import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'

import * as Styled from './styles/TeamMember.style'

import { TeamMemberProps } from './TeamMember.types'

const TeamMember: FC<TeamMemberProps> = ({
  name,
  role,
  picture,
}: TeamMemberProps): ReactElement => {
  return (
    <Styled.TeamMember backgroundImage={picture}>
      <Styled.Content>
        <Heading size={2} text={name} noMargin />
        <Heading size={1} text={role} fontStyle='body' />
      </Styled.Content>
    </Styled.TeamMember>
  )
}

export default TeamMember
