export interface TeamMemberProps {
  name: string
  role: string
  picture: string
}
