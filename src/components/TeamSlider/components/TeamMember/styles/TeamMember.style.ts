import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledTeamMemberProps } from './TeamMember.style.types'

export const TeamMember = styled.div((props: StyledTeamMemberProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.white};
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  aspect-ratio: 2 / 2.75;
  position: relative;
  overflow: hidden;
  background: url(${props.backgroundImage}) center center no-repeat;
  background-size: cover;

  @supports not (aspect-ratio: 2 / 2.75) {
    &::before {
      float: left;
      padding-top: 125%;
      content: '';
    }
    &::after {
      display: block;
      content: '';
      clear: both;
    }
  }

  &::after {
    content: '';
    display: block;
    position: absolute;
    top: 50%;
    left: 0;
    bottom: 0;
    right: 0;
    background: linear-gradient(to bottom, rgba(19, 20, 21, 0) 0%, rgba(19, 20, 21, .75) 100%);
  }
`)

export const Content = styled.div((props: StyledTeamMemberProps): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  align-items: flex-start;
  justify-content: flex-end;
  flex-direction: column;
  padding: ${props.theme.spacing.fixed[2]}px;
  color: ${props.theme.colours.white};
  z-index: 10;

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[6]}px;
  `)}
`)
