import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledTeamSliderProps } from './TeamSlider.style.types'

export const TeamSlider = styled.div((props: StyledTeamSliderProps): FlattenSimpleInterpolation => css`
  width: 100%;
  margin: ${props.theme.spacing.fixed[4]}px -${props.theme.spacing.fixed[2]}px 0;
  position: relative;

  .slick-list {
    overflow: visible;
  }

  ${props.theme.mixins.respondTo.md(css`
    width: unset;
    margin: ${props.theme.spacing.fixed[8]}px -${props.theme.spacing.fixed[2]}px 0;

    .slick-list {
      overflow: hidden;
    }
  `)}
`)

export const Slide = styled.div((props: StyledTeamSliderProps): FlattenSimpleInterpolation => css`
  padding: 0 ${props.theme.spacing.fixed[2]}px;
`)
