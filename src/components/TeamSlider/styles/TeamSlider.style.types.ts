import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledTeamSliderProps {
  theme: Theme;
}
