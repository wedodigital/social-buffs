import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledRawHtmlWrapperProps {
  theme: Theme
}
