import React, { FC, ReactElement } from 'react'

import { RawHtmlWrapperProps } from './RawHtmlWrapper.types'

import * as Styled from './styles/RawHtmlWrapper.style'

const RawHtmlWrapper: FC<RawHtmlWrapperProps> = ({
  content,
}: RawHtmlWrapperProps): ReactElement => {
  return <Styled.RawHtmlWrapper dangerouslySetInnerHTML={{ __html: content }} />
}

export default RawHtmlWrapper
