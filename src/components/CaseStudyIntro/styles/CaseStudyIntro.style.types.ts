import { Theme } from '@themes/sbTheme/sbTheme.types'

export interface StyledCaseStudyIntroProps {
  theme: Theme;
}
