import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledCaseStudyIntroProps } from './CaseStudyIntro.style.types'

export const Columns = styled.div((props: StyledCaseStudyIntroProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-direction: column;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
  `)}
`)

export const Content = styled.div((props: StyledCaseStudyIntroProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[6]}px;
  
  ${props.theme.mixins.respondTo.md(css`
    width: 66%;
    margin-bottom: 0;
  `)}
`)

export const Meta = styled.div((props: StyledCaseStudyIntroProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`
    width: calc(34% - ${props.theme.spacing.fixed[6]}px);
    margin-left: ${props.theme.spacing.fixed[6]}px;
  `)}
`)

export const Intro = styled.div((props: StyledCaseStudyIntroProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.white};
  padding-top: ${props.theme.spacing.fixed[8]}px;
  margin-bottom: ${props.theme.spacing.fixed[6]}px;
  font-size: ${props.theme.typography.heading[2].fontSize};
  line-height: ${props.theme.typography.heading[2].lineHeight};
  font-family: ${props.theme.typography.fontFamily.feature};

  ${props.theme.mixins.respondTo.md(css`
    width: 66%;
  `)}
`)

export const ContentBlock = styled.div((props: StyledCaseStudyIntroProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[6]}px;

  &:last-child {
    margin-bottom: 0;
  }
`)
