import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/ServiceGrid.style'

import { ServiceGridProps } from './ServiceGrid.types'

const ServiceGrid: FC<ServiceGridProps> = ({
  services,
}: ServiceGridProps): ReactElement => {
  return (
    <Styled.ServiceGrid>
      {
        services.map((service, index) => {
          return (
            <Styled.Service key={index}>
              <Styled.ServiceHeading>
                <Heading text={service.title} size={2} />
              </Styled.ServiceHeading>
              <RawHtmlWrapper content={service.content} />
            </Styled.Service>
          )
        })
      }
    </Styled.ServiceGrid>
  )
}

export default ServiceGrid
