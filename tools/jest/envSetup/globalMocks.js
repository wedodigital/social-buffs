import React from 'react'
import Enzyme from 'enzyme'

// define globals
global.React = React
global.mount = () => {
  console.error('DO NOT USE!')
}
global.shallow = Enzyme.shallow
global.render = Enzyme.render
global.snapshot = () => {
  console.error('DO NOT USE')
}
window.matchMedia = jest.fn().mockImplementation(query => {
  return {
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn()
  }
})
